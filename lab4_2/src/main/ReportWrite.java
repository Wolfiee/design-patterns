package main;

import main.exceptions.Error;
import main.exceptions.ErrorLog;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
public class ReportWrite {

    public void write(String name, Collection<Schedule> schedules){
        File file=new File(name);
        try(FileWriter writer = new FileWriter(file, false))
        {
            for(Schedule sc:schedules){
                writer.append(sc.toString());
                writer.append('\n');
            }
            writer.flush();
        }
        catch(IOException ex){
            ErrorLog.write( new main.exceptions.IOException(new Error()));
            file.delete();
        }
    }
}
