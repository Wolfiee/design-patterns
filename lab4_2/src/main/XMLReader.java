package main;
import main.exceptions.Error;
import main.exceptions.ErrorLog;
import main.exceptions.IOException;
import main.exceptions.XMLException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Андрей Матвиевич on 08.03.17.
 */

public class XMLReader {

     public Collection<Schedule> read(String file) {
        Collection<Schedule> schedules=new ArrayList<>();
        if(file != null) {
            try {
                InputStream stream = new FileInputStream(file);
                XMLInputFactory factory = XMLInputFactory.newInstance();
                XMLStreamReader reader = factory
                        .createXMLStreamReader(stream);
                Schedule schedule=null;
                while(reader.hasNext()) {
                    int event = reader.next();
                    switch(event) {
                        case XMLStreamReader.START_ELEMENT:
                            switch(reader.getLocalName()) {
                                case "schedule":
                                    schedule = new Schedule();
                                    break;
                                case "name":
                                    assert schedule != null;
                                    schedule.setName(reader.getElementText());
                                    break;
                                case "type":
                                    assert schedule != null;
                                    schedule.setType(reader.getElementText());
                                    break;
                                case "date":
                                    assert schedule != null;
                                    schedule.setDate(new SimpleDateFormat("dd.MM.yyyy").parse(reader.getElementText()));
                                    break;
                                case "time":
                                    assert schedule != null;
                                    schedule.setTime(new SimpleDateFormat("HH:mm").parse(reader.getElementText()));
                                    break;
                                case "hour":
                                    assert schedule != null;
                                    schedule.setHour(new SimpleDateFormat("HH").parse(reader.getElementText()));
                                    break;
                                case "number":
                                    assert schedule != null;
                                    schedule.setNumber(Integer.parseInt(reader.getElementText()));
                                    break;
                            }
                            break;
                        case XMLStreamReader.END_ELEMENT:
                            if("schedule".equals(reader.getLocalName())) {
                                schedules.add(schedule);
                            }
                            break;
                    }
                }
                reader.close();
            } catch(FileNotFoundException e) {

                ErrorLog.write(new IOException(new Error()));
            } catch(XMLStreamException e) {
                ErrorLog.write(new XMLException(new Error()));
            } catch (ParseException p){
                ErrorLog.write(new main.exceptions.ParseException(new Error()));
            }
        }
        return schedules;
    }
}
