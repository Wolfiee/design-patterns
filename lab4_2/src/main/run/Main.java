package main.run;

import main.*;
import main.report.CabinetReport;
import main.report.DateReport;
import main.report.Report;

import java.util.Collection;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    XMLReader reader=new XMLReader();
	    Collection<Schedule> schedules=reader.read("schedule.xml");
        System.out.println(schedules);

        Scanner scanner=new Scanner(System.in);
        System.out.println("1: создание отчёта №1");
        System.out.println("2: создание отчёта №1");
        System.out.println("3: создание отчёта №1");
        int menu=scanner.nextInt();
        ReportWrite rw=new ReportWrite();
        Report sr;
        switch (menu){
            case 1:{
                sr=new SubjectReport();
                System.out.println("Введите название предмета");
                String name=scanner.next();
                System.out.println("введите имя файла отчёта");
                String file=scanner.next();
                rw.write(file+".txt",sr.generate(name,schedules));
                break;
            }
            case 2: {
                sr = new DateReport();
                System.out.println("Введите дату в формате dd.MM.yyyy");
                String name = scanner.next();
                System.out.println("введите имя файла отчёта");
                String file = scanner.next();
                rw.write(file + ".txt", sr.generate(name, schedules));
                break;
            }
            case 3: {
                sr = new CabinetReport();
                System.out.println("Введите номер аудитории");
                String name = scanner.next();
                System.out.println("введите имя файла отчёта");
                String file = scanner.next();
                rw.write(file + ".txt", sr.generate(name, schedules));
                break;
            }
        }
    }
}
