package main.report;

import main.Schedule;
import main.comparator.TimeComparator;
import main.exceptions.Error;

import java.text.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
public class DateReport implements Report {
    @Override
    public Collection<Schedule> generate(String date, Collection<Schedule> schedules) {
        Collection<Schedule> subject=new ArrayList<>();
        try {
            new SimpleDateFormat("dd.MM.yyyy").parse(date);
        } catch (java.text.ParseException e) {
            new main.exceptions.ParseException(new Error()).print();
            return subject;
        }
        for (Schedule sc:schedules) {
            if((new SimpleDateFormat("dd.MM.yyyy").format(sc.getDate())).equals(date)){
                subject.add(sc);
            }
        }
        ((List<Schedule>) subject).sort(new TimeComparator());
        return subject;
    }
}
