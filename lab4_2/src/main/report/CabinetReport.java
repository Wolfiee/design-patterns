package main.report;

import main.Schedule;
import main.comparator.DateComparator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
public class CabinetReport implements Report {
    @Override
    public Collection<Schedule> generate(String s, Collection<Schedule> schedules) {
        Collection<Schedule> subject=new ArrayList<>();
        for (Schedule sc:schedules) {
            if(sc.getNumber()==Integer.parseInt(s)){
                subject.add(sc);
            }
        }
        ((List<Schedule>) subject).sort(new DateComparator());
        return subject;
    }
}
