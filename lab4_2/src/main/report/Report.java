package main.report;

import main.Schedule;

import java.util.Collection;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
public interface Report {
    Collection<Schedule> generate(String s, Collection<Schedule> sc);
}
