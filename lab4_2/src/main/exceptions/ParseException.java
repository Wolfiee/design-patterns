package main.exceptions;

import main.Decorator;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
public class ParseException extends Decorator {
    public ParseException(Errors c) {
        super(c);
    }
    @Override
    public void print(){
        System.err.print("Неверный формат даты (dd.MM.yyyy) ");
        ErrorLog.message+="Неверный формат даты (dd.MM.yyyy) ";
        super.print();
    }
}
