package main.exceptions;

import java.io.FileWriter;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
public  class ErrorLog{
    public static String  message="";

       public static void write(Errors er){
            try(FileWriter writer = new FileWriter("errors.txt", true))
            {
                er.print();
                writer.append(message);
                writer.append('\n');
                writer.flush();
                message="";
            }
            catch(java.io.IOException ex){
                new IOException(new Error()).print();
            }
        }
}
