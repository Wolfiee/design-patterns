package main.exceptions;

import main.Decorator;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
public class IOException extends Decorator {
    public IOException(Errors c) {
        super(c);
    }
    @Override
    public void print(){
        System.err.print("Ошибка в работе с файлом ");
        ErrorLog.message+="Ошибка в работе с файлом ";
        super.print();
    }
}
