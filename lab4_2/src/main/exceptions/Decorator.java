package main.exceptions;

import main.exceptions.Errors;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
abstract class Decorator implements Errors {
    protected Errors component;

    public Decorator (Errors c) {
        component = c;
    }

    @Override
    public void print() {
        component.print();
    }

}
