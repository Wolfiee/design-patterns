package main.exceptions;


/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
public class XMLException extends Decorator {
    public XMLException(Errors c) {
        super(c);
    }
    @Override
    public void print(){
        System.err.print("Ошибка в работе с XML файлом ");
        ErrorLog.message+="Ошибка в работе с XML файлом ";
        super.print();
    }
}
