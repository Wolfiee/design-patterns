package main.comparator;

import main.Schedule;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
public class TimeComparator implements java.util.Comparator<Schedule> {
    @Override
    public int compare(Schedule o1, Schedule o2) {
        return o1.getTime().compareTo(o2.getTime());
    }
}
