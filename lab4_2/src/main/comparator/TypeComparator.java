package main.comparator;

import main.Schedule;

import java.util.Comparator;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
public class TypeComparator implements Comparator<Schedule> {

    @Override
    public int compare(Schedule p1, Schedule p2) {
        return p1.getType().compareToIgnoreCase(p2.getType());
    }
}