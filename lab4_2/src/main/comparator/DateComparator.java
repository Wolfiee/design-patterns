package main.comparator;

import main.Schedule;

import java.util.Comparator;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
public class DateComparator implements Comparator<Schedule> {
    @Override
    public int compare(Schedule o1, Schedule o2) {
        return o1.getDate().compareTo(o2.getDate());
    }
}
