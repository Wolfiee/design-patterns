package main;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
public class Schedule {
    private String name;
    private String type;
    private Date date;
    private Date time;
    private Date hour;
    private int number;

    public Schedule(String name, String type, Date date, Date time, Date hour, int number) {
        this.name = name;
        this.type = type;
        this.date = date;
        this.time = time;
        this.hour = hour;
        this.number = number;
    }
    public Schedule(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getHour() {
        return hour;
    }

    public void setHour(Date hour) {
        this.hour = hour;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return name+" "+type+" "+new SimpleDateFormat("dd.MM.yyyy").format(date)+" "+new SimpleDateFormat("HH:mm").format(time)+" "+
                new SimpleDateFormat("HH").format(hour)+" "+number;
    }
}
