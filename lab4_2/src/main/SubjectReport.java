package main;

import main.comparator.TypeComparator;
import main.report.Report;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
public class SubjectReport implements Report {

    public Collection<Schedule> generate(String name, Collection<Schedule> schedules){
        Collection<Schedule> subject=new ArrayList<>();
        for (Schedule sc:schedules) {
            if(sc.getName().equals(name)){
                subject.add(sc);
            }
        }
        ((List<Schedule>) subject).sort(new TypeComparator());
        return subject;
    }
}
