import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Created by Андрей Матвиевич on 26.02.17.
 */
public class CardsStackMethods {

    private CardsStack remove(CardsStack c) {
        try {
            c.getStack().remove(c.getLastCard());
            c.getLastCard().open();
        } catch (NoSuchElementException ex) {
        }
        return c;
    }

    private boolean compare(CardsStack cs1, CardsStack cs2) {
        return Objects.equals(cs1.getLastCard().getRank(), cs2.getLastCard().getRank());
    }

    public List<CardsStack> delete(List<CardsStack> field, int key1, int key2) {
        try {
            CardsStack cs1 = field.get(key1);

            CardsStack cs2 = field.get(key2);
            if (compare(cs1, cs2)) {
                remove(cs1);
                remove(cs2);
            } else {
                System.out.println("Значения не совпадают");
            }
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("Значения не совпадают");
        }
        return field;

    }
}
