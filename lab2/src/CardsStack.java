import java.util.Stack;

/**
 * Created by Андрей Матвиевич on 26.02.17.
 */
public class CardsStack {
    private Stack<Card> cards;

    public CardsStack(Deck deck){
        cards=new Stack<>();
        DeckMethods me=new DeckMethods();
        for (int i = 0; i < 4 ; i++) {
            cards.push(me.getFirstCard(deck));
        }
    }

    public Stack<Card> getStack() {
        return cards;
    }
    public Card getLastCard(){
        return cards.lastElement();
    }

    @Override
    public String toString() {
        return cards.toString();
    }
}
