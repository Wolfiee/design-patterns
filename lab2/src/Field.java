import java.util.ArrayList;
import java.util.List;

/**
 * Created by Андрей Матвиевич on 26.02.17.
 */
public class Field {
    List<CardsStack> field;

    public Field(Deck deck){
        field=new ArrayList<>();
        for (int i = 0; i < 9 ; i++) {
            CardsStack c=new CardsStack(deck);
            c.getLastCard().open();
            field.add(c);
        }
    }

    public List<CardsStack> getField() {
        return field;
    }

    public void setField(List<CardsStack> field) {
        this.field = field;
    }

}
