import java.util.Collections;
import java.util.Stack;

/**
 * Created by Андрей Матвиевич on 26.02.17.
 */
public class Deck {

    private Stack<Card> cards;

    public Deck(){
        cards=new Stack<>();
        for (String rank : CardsInfo.ranks) {
            for (String suit : CardsInfo.suits) {
                cards.push(new Card(rank,suit));
            }
        }
    }

    public Stack<Card> getCards() {
        return cards;
    }

    @Override
    public String toString() {
        return cards.toString();
    }
}
