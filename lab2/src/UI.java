
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Андрей Матвиевич on 26.02.17.
 */
public class UI {

    public UI(){
        System.out.println("Управление осуществляется посредством ввода номера столбика содержащего нужную карту" +
                "(номера столбцов располагаются сверху)");
        System.out.println("Игра заканчивается когда все карты будут убраны с поля");
    }

    public List<Integer> UserCommands(Scanner sc){
        int key1,key2;
        while (true) {
            System.out.println("Введите первый номер");
            key1 = sc.nextInt();
            System.out.println("Введите второй номер");
            key2 = sc.nextInt();
            if(key1!=key2)break;
            else System.out.println("Нене");
        }
        List<Integer> commands=new ArrayList<>();
        commands.add(key1-1);
        commands.add(key2-1);
        return commands;
    }
}
