import java.util.Collection;

/**
 * Created by Андрей Матвиевич on 26.02.17.
 */
public class PrintField {

    public void printField(Field field){
        for (int i = 0; i <field.getField().size();i++) {
            System.out.print(String.format("%9s",i+1));
        }
        System.out.println();
        for (int i = 0; i <getMaxStackSize(field) ; i++) {
            for (int j = 0; j < field.getField().size(); j++) {
                if(field.getField().get(i)!=null){
                    if(field.getField().get(j).getStack().size()<=i){
                        System.out.print(String.format("%8s"," "));
                }
                    else {
                        System.out.print(String.format("%8s", field.getField().get(j).getStack().get(i)));
                    }
                    //System.out.print(field.getField().get(j).getStack().get(i)+"    ");
                }
            }
            System.out.println();
        }
    }
    private int getMaxStackSize(Field field){
        int size=0;
        for(CardsStack cs:field.getField()){
            if(cs.getStack().size()>size){
                size=cs.getStack().size();
            }
        }
        return size;
    }
}
