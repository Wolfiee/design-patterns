import java.util.Arrays;

/**
 * Created by Андрей Матвиевич on 26.02.17.
 */
public class Card {
    private String rank;
    private String suit;
    private boolean open;

    public Card(String r, String s) {
            this.rank = r;
            this.suit = s;
            this.open=false;
    }

    public String getRank() {
        return rank;
    }

    public String getSuit() {
        return suit;
    }

    public void close(){
        open=false;
    }

    public void open(){
        open=true;
    }

    @Override
    public String toString() {
        if(open) return getRank()+getSuit();
        else return "⬛";
    }
}
