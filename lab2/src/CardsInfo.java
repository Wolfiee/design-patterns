/**
 * Created by Андрей Матвиевич on 26.02.17.
 */
public class CardsInfo {
    public static final String[] ranks = {
            "6",
            "7",
            "8",
            "9",
            "10",
            "Jack",
            "Queen",
            "King",
            "Ace"
    };
    public static final String[] suits = {
            "♥",
            "♦",
            "♣",
            "♠"
    };
}
