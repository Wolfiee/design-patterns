import java.util.Collections;
import java.util.List;

/**
 * Created by Андрей Матвиевич on 26.02.17.
 */
public class DeckMethods {
    public Deck shuffle(Deck deck){
        Collections.shuffle(deck.getCards());
        return deck;
    }
    public Card getFirstCard(Deck deck){
        Card card=deck.getCards().firstElement();
        deck.getCards().remove(card);
        return card;
    }
}
