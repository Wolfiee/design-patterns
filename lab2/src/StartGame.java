import java.util.List;
import java.util.Scanner;

/**
 * Created by Андрей Матвиевич on 26.02.17.
 */
public class StartGame {

    public StartGame(){

        Deck deck=new Deck();
        DeckMethods me=new DeckMethods();
        me.shuffle(deck);

        Field field=new Field(deck);
        PrintField pf=new PrintField();
        pf.printField(field);
        CardsStackMethods csm=new CardsStackMethods();
        UI user=new UI();
        Scanner sc=new Scanner(System.in);
        boolean win=false;
        while (true) {
            List<Integer> commands = user.UserCommands(sc);
            csm.delete(field.getField(), commands.get(0), commands.get(1));
            field=deleteEmpty(field);
            pf.printField(field);
            if(canPlay(field)==1){
                win=true;
                break;
            }
            if(canPlay(field)==0){
                System.out.println("Не осталось ходов");
                break;
            }
        }
        if(win) System.out.println("Пасьянс сошёлся");
        else System.out.println("Пасьянс не сошёлся");

    }
    private Field deleteEmpty(Field field){
        for(CardsStack cs:field.getField()){
            if(cs.getStack().isEmpty()){
                field.getField().remove(cs);
                break;
            }
        }
        for(CardsStack cs:field.getField()){
            if(cs.getStack().isEmpty()){
                field.getField().remove(cs);
                break;
            }
        }
        return field;
    }

    private int canPlay(Field field){
        if(field.getField().isEmpty())
            return 1;
        for (int i = 0; i <field.getField().size() ; i++) {
            for (int j = i+1; j <field.getField().size() ; j++) {
                if(field.getField().get(i).getLastCard().getRank().equals(field.getField().get(j).getLastCard().getRank())){
                    return 2;
                }
            }
        }
        return 0;
    }

}
