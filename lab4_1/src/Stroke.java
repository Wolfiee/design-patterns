import java.awt.BasicStroke;
import java.awt.Graphics2D;

public class Stroke extends Decorator {
	private java.awt.Stroke stroke;

	public Stroke(Figure f,int width) {
		super(f);
		stroke = new BasicStroke((float)width);
	}

	public void paint(Graphics2D g) {
		g.setStroke(stroke);
		super.paint(g);
	}
}
