import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.MultipleGradientPaint;
import java.awt.RadialGradientPaint;

public class RadialGradient {
	private MultipleGradientPaint gradient;

	public RadialGradient(Circle circle, Color centerColor, Color borderColor) {
		gradient = new RadialGradientPaint(circle.getCenter().getX(), circle.getCenter().getY(), circle.getRadius(), new float[] {0, 1}, new Color[] {centerColor, borderColor});
	}

	public MultipleGradientPaint getGradient() {
		return gradient;
	}
}

class RadialGradientDecorator extends Decorator{
	private RadialGradient rg;

	RadialGradientDecorator(Figure f,RadialGradient rg){
		super(f);
		this.rg=rg;
	}

	public void paint(Graphics2D g) {
		g.setPaint(rg.getGradient());
		super.paint(g);
	}
}