import java.awt.*;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
public class Ellipse implements Figure {
    private Point center;
    private int w;
    private int x;
    private int y;
    private int h;

    Ellipse(Point center,int w,int h){
        this.center = center;
        this.w = w;
        x = center.getX();
        y = center.getY();
        this.h = h;
    }


    @Override
    public void paint(Graphics2D g) {
        g.fillOval(x,y,w,h);
    }
}
