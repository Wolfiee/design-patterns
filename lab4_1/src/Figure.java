import java.awt.*;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
public interface Figure {
    public void paint(Graphics2D g);
}
