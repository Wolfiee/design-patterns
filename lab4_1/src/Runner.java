import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Runner {
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		Collection<Figure> figures = new ArrayList<>();
        LinearGradient lg=new LinearGradient(new Line(new Point(0, 250), new Point(0, 350)), new Color(0, 128, 255), new Color(0, 0, 128));

		figures.add(new LinearGradientDecorator(new Rectangle(new Point(275, 300), 550, 300),lg));
		figures.add(new Colour(new Circle(new Point(450, 50), 25),Color.YELLOW));

        figures.add(new Colour(new Polygon(new Point(215, 210), new Point(215, 170), new Point(190, 170), new Point(190, 180),new Point(205, 180),new Point(205, 210)),Color.ORANGE));
        figures.add(new Colour(new Polygon(new Point(325, 298), new Point(395, 298), new Point(395, 250)),Color.BLACK));
		figures.add(new Colour(new Ellipse(new Point(155,200),250,100), new Color(255,255,102)));


		RadialGradient rg=new RadialGradient(new Circle(new Point(205, 250), 22), Color.CYAN, Color.WHITE);
		figures.add(new RadialGradientDecorator(new Circle(new Point(205, 250), 23),rg));
		figures.add(new Colour(new Stroke(new CircleBorder(new Point(205, 250), 23),3),Color.MAGENTA));

        rg=new RadialGradient(new Circle(new Point(285, 250), 19), Color.CYAN, Color.WHITE);
        figures.add(new RadialGradientDecorator(new Circle(new Point(285, 250), 15),rg));
        figures.add(new Colour(new Stroke(new CircleBorder(new Point(285, 250), 15),3),Color.MAGENTA));

        rg=new RadialGradient(new Circle(new Point(365, 250), 19), Color.CYAN, Color.WHITE);
        figures.add(new RadialGradientDecorator(new Circle(new Point(365, 250), 15),rg));
        figures.add(new Colour(new Stroke(new CircleBorder(new Point(365, 250), 15),3),Color.MAGENTA));

		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		new MainFrame("Рисунок", 550, 350, new Picture(figures));
	}
}
