import java.awt.Color;
import java.awt.Graphics2D;

public class Colour extends Decorator {
	private Color color;

	public Colour(Figure f,Color color) {
		super(f);
		this.color = color;
	}

	public void paint(Graphics2D g) {
		g.setColor(color);
		super.paint(g);
	}
}
