import java.awt.*;

/**
 * Created by Андрей Матвиевич on 21.03.17.
 */
abstract class Decorator implements Figure {
    protected Figure component;

    public Decorator (Figure c) {
        component = c;
    }


    @Override
    public void paint(Graphics2D g) {
        component.paint(g);
    }
}
