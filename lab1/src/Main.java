import java.io.IOException;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args)throws IOException {
        CSVReader r=new CSVReader();
        CSVWriter w=new CSVWriter();
        Algorithm alg=new Algorithm();
        List<SellData> selldatas=r.read("sell.csv");
        List<Feedback> feedbacks=alg.Alg(selldatas);
        feedbacks.sort(new GainSort());
        w.write(feedbacks);
    }
}
