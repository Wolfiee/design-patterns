import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by 1401090033 on 13.02.2017.
 */
public class CSVWriter {

    void write(List<Feedback> f) throws IOException {
        FileWriter w = new FileWriter("feedback.csv");

        for (Feedback value : f) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getName());
            sb.append(";");
            sb.append(value.getGain());
            sb.append("\n");
            System.out.println(sb);
            w.write(sb.toString());
        }
        w.close();
    }
}
