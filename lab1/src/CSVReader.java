import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 1401090033 on 13.02.2017.
 */
public class CSVReader {
    List<SellData> read(String csv) throws IOException {
        List<SellData> sd=new ArrayList<>();
        BufferedReader br;
        br = new BufferedReader(new FileReader(csv));
        String line;
        String cvsSplitBy = ";";
        while ((line = br.readLine()) != null) {
            String[] temp = line.split(cvsSplitBy);
            SellData product= new SellData();
            product.setName(temp[0]);
            product.setCost(Double.parseDouble(temp[1]));
            product.setCount(Integer.parseInt(temp[2]));
            sd.add(product);
        }
        return sd;
    }
}
