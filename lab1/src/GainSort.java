import java.util.Comparator;

/**
 * Created by 1401090033 on 13.02.2017.
 */
public class GainSort implements Comparator<Feedback> {

    @Override
    public int compare(Feedback f1, Feedback f2) {
        if(f1.getGain()>f2.getGain()) return 1;
        if(f1.getGain()<f2.getGain()) return -1;
        else return 0;
    }
}
