import java.util.ArrayList;
import java.util.List;

/**
 * Created by 1401090033 on 13.02.2017.
 */
public class Algorithm {
    List<Feedback> Alg(List<SellData> sd){
        List<Feedback> fs=new ArrayList<>();
        List<SellData> used=new ArrayList<>();
        boolean flag;
        for (SellData d:sd) {
            flag=true;
            for(SellData a:used){
                if(a.getName().equals(d.getName())){
                    flag=false;
                    break;
                }
            }
            if(!flag){
                for(Feedback f:fs){
                    if(f.getName().equals(d.getName())){
                        f.setGain(f.getGain()+d.getCount()*d.getCost());
                        break;
                    }
                }
            }
            else{
                Feedback f=new Feedback();
                f.setName(d.getName());
                f.setGain(d.getCount()*d.getCost());
                fs.add(f);
                used.add(d);
            }

        }
        return fs;
    }
}
