/**
 * Created by 1401090033 on 13.02.2017.
 */
public class Feedback {
    String name;
    double gain;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getGain() {
        return gain;
    }

    public void setGain(double gain) {
        this.gain = gain;
    }
}
