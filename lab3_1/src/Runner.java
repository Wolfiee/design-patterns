import java.awt.*;
import java.util.Comparator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

public class Runner {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {}
        JFrame window = new JFrame("Главное окно");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setSize(700, 400);
        List<Person> persons = PersonXmlReader.readFromFile("persons.xml");
        PersonTableModel model = new PersonTableModel(persons);
        JTable table = new JTable(model);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        resizeColumnWidth(table);
        JScrollPane scrollPane = new JScrollPane(table);
        window.add(scrollPane, BorderLayout.CENTER);
        JPanel chooser = new JPanel(new BorderLayout());
        JComboBox<Comparator<Person>> comboBox = new JComboBox<>();
        comboBox.addItem(new DateComparator());
        comboBox.addItem(new DayOfWeekComparator());
        comboBox.addItem(new MonthComparator());
        comboBox.addItem(new StreetComparator());
        chooser.add(comboBox, BorderLayout.CENTER);
        JButton sortButton = new JButton("сортировать");
        sortButton.addActionListener(new SortButtonListener(comboBox, model));
        chooser.add(sortButton, BorderLayout.EAST);
        window.add(chooser, BorderLayout.NORTH);
        window.setVisible(true);
        window.setLocationRelativeTo(null);
    }
    public static void resizeColumnWidth(JTable table) {
        final TableColumnModel columnModel = table.getColumnModel();
        for (int column = 0; column < table.getColumnCount(); column++) {
            int width = 80; // Min width
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width +1 , width);
            }
            if(width > 700)
                width=700;
            columnModel.getColumn(column).setPreferredWidth(width);
        }
    }
}