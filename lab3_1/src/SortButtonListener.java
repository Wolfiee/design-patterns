import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JComboBox;

public class SortButtonListener implements ActionListener {
    private JComboBox<Comparator<Person>> comboBox;
    private PersonTableModel model;

    public SortButtonListener(JComboBox<Comparator<Person>> comboBox, PersonTableModel model) {
        this.comboBox = comboBox;
        this.model = model;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Collections.sort(model.getPersons(),(Comparator<Person>)comboBox.getSelectedItem());
        model.update();
    }
}
