import java.util.Comparator;

/**
 * Created by Андрей Матвиевич on 08.03.17.
 */
public class StreetComparator implements Comparator<Person> {
    @Override
    public int compare(Person p1, Person p2) {
        String temp[]=p1.getMail().split(",");
        String temp1[]=p2.getMail().split(",");
        return Integer.compare(temp[0].length(),temp1[0].length());
    }

    @Override
    public String toString() {
        return "по количеству символов в названии улицы";
    }
}
