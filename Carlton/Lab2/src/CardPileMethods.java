import java.util.Deque;

public class CardPileMethods {

    public Table check(Table field, int[] key1, int key2) {
        CardPile pile1 = field.getField().get(key1[0]);
        CardPile pile2 = field.getField().get(key2);

        if (pile2.getCards().isEmpty() && pile1.getCards().get(key1[1]).getValue().equals("King")) {

            pereklad(pile1,pile2,key1,key2);
            return field;
            //проверить упорядочена ли стопка или если король
            //вызвать метод переноса карт
        }
        else {
            if (pile2.getCards().isEmpty()) {
                return field;
            }
        }


        if (pile1.getCards().get(key1[1]).getColor().equals(pile2.getCards().lastElement().getColor())) {
            System.out.println("Карты одинаковых цветов");
            return field;
        }
        CardMethods cm = new CardMethods();

        //метод проверки упорядоченности
        if (!order(key1,cm,pile1)) {
            System.out.println("Выбрана неупорядоченая стопка");
        } else {
            if (cm.getFace(pile2.getCards().get(pile2.getCards().size() - 1).getValue()) - 1 == cm.getFace(pile1.getCards().get(key1[1]).getValue())) {
                pereklad(pile1,pile2,key1,key2);
            }//отдельный метод по переносу карт
            else System.out.println("Значения не упорядочены");

        }

        return field;
    }

    public void pereklad(CardPile pile1,CardPile pile2,int[] key1, int key2) {
        for (int i = key1[1]; i < pile1.getCards().size(); i++) {
            pile2.getCards().push(pile1.getCards().elementAt(i));
            //pile1.getCards().removeElementAt(i-j);
        }
        while (pile1.getCards().size() > key1[1]) {
            pile1.getCards().pop();
        }
    }
    public boolean order(int[] key1,CardMethods cm ,CardPile pile1)
    {
        for (int i = key1[1]; i < pile1.getCards().size() - 2; i++) {
            if (cm.getFace(pile1.getCards().get(i).getValue()) != cm.getFace(pile1.getCards().get(i + 1).getValue() + 1)) {
                return false;
            }
        }
        return true;
    }

}
