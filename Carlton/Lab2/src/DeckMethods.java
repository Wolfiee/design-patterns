
public class DeckMethods extends Deck {

    public Card getFirstCard(Deck deck){
        return deck.getCards().pop();
    }
    public Table addCards(Table table,Deck deck){//Колода(добавление в конец всем стопкам)
        if(!deck.getCards().isEmpty()){
        for (CardPile pile:table.getField()){
            pile.getCards().push(getFirstCard(deck));
        }
        return table;
        }
        else {
            System.out.println("Колода пуста");
            return table;
        }
    }
}