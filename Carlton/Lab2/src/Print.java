import java.util.List;

public class Print extends Deck {

    public void printTable(Table table, List<Foundation> foundation) {

        for (int i = 0; i < 4; i++) {
            if(foundation.get(i).getFcards().isEmpty())
            {
                System.out.print("F" + i + ": ");
                System.out.print("Empty\t");
            }else System.out.print(foundation.get(i).getFcards().get(foundation.get(i).getFcards().size()-1)+ "\t");


            // System.out.print("F" + i + ": ");
        }
        System.out.println("\n");


        for (int j = 0; j < 4; j++) {
            System.out.print("          "+j);
        }
        System.out.println();
        for (int i = 0; i < 4; i++) {
                System.out.println("L" + i + ":  " + table.getField().get(i));
            }

        System.out.println("\n");
    }
}