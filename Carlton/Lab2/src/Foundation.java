import java.util.Stack;

public class Foundation {

    private Stack<Card> fcards;

    public Foundation()
    {
        fcards = new Stack<>();
    }

    public boolean addCard(Card card) {
        if (card.getValue().equals("Ace") && fcards.isEmpty()) {
            System.out.println("true");
            fcards.push(card);
            return true;
        }

        CardMethods cardMethods = new CardMethods();
        if (!fcards.isEmpty() && cardMethods.getFace(fcards.get(0).getValue()) + 1 == cardMethods.getFace(card.getValue()))//v rot mne+1
        {
            if (fcards.get(0).getSuit().equals(card.getSuit())) {
                fcards.push(card);
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return fcards.toString();
    }

    public Stack<Card> getFcards() {
        return fcards;
    }

    public boolean solved() {
        return !fcards.isEmpty() && fcards.lastElement().getValue().equals("King");
    }
}