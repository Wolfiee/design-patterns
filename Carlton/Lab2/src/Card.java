import java.text.ParsePosition;
import java.util.Objects;


public class Card extends CardProperties {
    private String value, suit, color;

    Card(String value, String suit, String color) {
        if (!Objects.equals(suits[1], "♠") &&
                !Objects.equals(suits[0], "♥") &&
                !Objects.equals(suits[2], "♦") &&
                !Objects.equals(suits[3], "♣"))
            throw new IllegalArgumentException("Illegal playing card suit");
//        if (Integer.parseInt(value) < 2 || Integer.parseInt(value) > 13)
//            throw new IllegalArgumentException("Illegal playing card value");

        this.value = value;
        this.suit = suit;
        this.color = color;
    }

    @Override
    public String toString() {
        return value + suit + "(" + color + ") ";
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSuit() {
        return suit;
    }

    public void setSuit(String suit) {
        this.suit = suit;
    }


}