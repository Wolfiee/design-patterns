import java.util.Stack;

public class CardPile {
    private Stack<Card> cards;

    public CardPile(Deck deck, int a) {
        cards = new Stack<>();
        DeckMethods me = new DeckMethods();
        for (int i = 0; i < a; i++) {
            cards.push(me.getFirstCard(deck));
        }
    }

    public Stack<Card> getCards() {
        return cards;
    }

    @Override
    public String toString() {
        return cards.toString();
    }

//fix
}