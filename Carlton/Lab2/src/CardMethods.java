import java.util.Objects;

public class CardMethods {
    public int getFace(String value) {
        if (Objects.equals(value, "Jack")) {
            return 10;
        } else if (Objects.equals(value, "Queen")) {
            return 11;
        } else if (Objects.equals(value, "King")) {
            return 12;
        } else if (Objects.equals(value, "Ace")) {
            return 1;
        }
        return Integer.parseInt(value);
    }
}