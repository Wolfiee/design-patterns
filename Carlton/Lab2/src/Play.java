import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Play {

    public Play() {
        Deck deck = new Deck();
        Reshuffler reshuffler = new Reshuffler();
        reshuffler.reshuffle(deck);
        System.out.println();
        Print print = new Print();
        Table table = new Table(deck);
        Foundation foundation = new Foundation();
        Foundation f1 = new Foundation();
        Foundation f2 = new Foundation();
        Foundation f3 = new Foundation();
        Foundation f4 = new Foundation();
        List<Foundation> listf = new ArrayList<>();
        listf.add(f1);
        listf.add(f2);
        listf.add(f3);
        listf.add(f4);

        table.getField().get(0).getCards().set(table.getField().get(0).getCards().size() - 2, new Card("King", "♠", "black"));
        table.getField().get(0).getCards().set(table.getField().get(0).getCards().size() - 1, new Card("Ace", "♠", "black"));
        table.getField().get(1).getCards().set(table.getField().get(1).getCards().size() - 1, new Card("2", "♠", "red"));
        table.getField().get(3).getCards().remove(0);
        // print.printTable(table, listf);
        //System.out.println();
        DeckMethods dm = new DeckMethods();

        run(listf, table, deck, dm, foundation, print);

    }

    public void run(List<Foundation> listf, Table table, Deck deck, DeckMethods dm, Foundation foundation, Print print) {
        Scanner sc = new Scanner(System.in);

        while (!solved(listf)) {
            print.printTable(table, listf);
            System.out.println("1.Клик по колоде выкладывает ещё один ряд карт.");
            System.out.println("2.Переложить карту со стопки на стопку");
            System.out.println("3.Положить карту на основание(находится сверху над стопками)");
            System.out.println("4.Изъять кату из основание(находится сверху над стопками)");
            System.out.println("5.Выход из игры.");

            System.out.print("\nВведите команду: ");
            int numcase = sc.nextInt();
            System.out.println();
            int col, row;

            switch (numcase) {
                case 1:
                    dm.addCards(table, deck);
                    break;
                case 2:
                    System.out.println("Введите место где находится карта");
                    System.out.print("Введите строку(0-4): ");
                    row = sc.nextInt();
                    System.out.print("Введите столбец(0-4): ");
                    col = sc.nextInt();
                    int[] command1 = {row, col};
                    System.out.println("\nВведите куда положить карту:");
                    int command2 = sc.nextInt();
                    CardPileMethods cpm = new CardPileMethods();
                    cpm.check(table, command1, command2);
                    break;
                case 3:
                    System.out.println("Введите в какой столбик добавить карту: ");
                    col = sc.nextInt();
                    System.out.println("Введите из какой строки взять карту: ");
                    row = sc.nextInt();

                    Card card = table.getField().get(row).getCards().get(table.getField().get(row).getCards().size() - 1);
                    System.out.println(card);
                    if (listf.get(col).addCard(card)) {
                        table.getField().get(row).getCards().pop();
                    }

                    System.out.println(foundation.getFcards().size());
                    //print.printTable(table, listf);
                    break;
                case 4:
                    System.out.println("Введите из какого столбика забирать карту: ");
                    col = sc.nextInt();
                    System.out.println("Введите в какую строку добавить карту: ");
                    row = sc.nextInt();

                    CardMethods cardMethods = new CardMethods();

                    if (!listf.get(col).getFcards().isEmpty() && cardMethods.getFace(listf.get(col).getFcards().lastElement().getValue()) + 1
                            == cardMethods.getFace(table.getField().get(row).getCards().lastElement().getValue())) {
                        if (!listf.get(col).getFcards().lastElement().getColor().equals(table.getField().get(row).getCards().lastElement().getColor())) {
                            table.getField().get(row).getCards().push(listf.get(col).getFcards().pop());
                        }
                    }
                    // print.printTable(table, listf);
                    break;
                case 5:
                    System.out.println("До встречи.");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Не корректно введены данные!!!");
                    System.out.println();
            }
        }
        System.out.println("Игра Завершена. Поздравляю!!!");
        sc.close();
    }

    public boolean solved(List<Foundation> listf) {
        return listf.get(0).solved() && listf.get(1).solved() && listf.get(2).solved() && listf.get(3).solved();
    }
}