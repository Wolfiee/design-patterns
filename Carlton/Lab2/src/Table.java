import java.util.ArrayList;
import java.util.List;

public class Table {
    List<CardPile> field;

    public Table(Deck deck) {

        field = new ArrayList<>();
        for (int i = 4; i > 0; i--) {
            CardPile c = new CardPile(deck, i);
            field.add(c);
    }
    }

    public List<CardPile> getField() {
        return field;
    }
}
