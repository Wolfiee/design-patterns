import java.util.Stack;

public class Deck {

    private Stack<Card> cards;

    public Deck(){
        cards=new Stack<>();
        for (String value : CardProperties.values) {
            for (String suit : CardProperties.suits) {
                    if (suit.equals("♥") || suit.equals("♦")) cards.push(new Card(value, suit, CardProperties.colors[0]));
                    else cards.push(new Card(value, suit, CardProperties.colors[1]));
            }
        }
    }

    public Stack<Card> getCards() {
        return cards;
    }

    @Override
    public String toString() {
        return cards.toString();
    }
}
