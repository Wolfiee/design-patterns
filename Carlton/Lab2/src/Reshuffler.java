import java.util.Collections;

public class Reshuffler {

    public Reshuffler() {
    }

    public void reshuffle(Deck pack) {
        Collections.shuffle(pack.getCards());
    }
}