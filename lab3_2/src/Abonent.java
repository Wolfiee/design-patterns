

/**
 * Created by Андрей Матвиевич on 08.03.17.
 */
public class Abonent {
    private final String phoneNumber;
    private final double balance;
    private final String name;


    private Abonent(AbonentBuilder abonentBuilder) {
        this.phoneNumber = abonentBuilder.phoneNumber;
        this.balance = abonentBuilder.balance;
        this.name = abonentBuilder.name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public double getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }



    @Override
    public String toString() {
        return phoneNumber+" "+String.format("%.2f",balance)+" "+name+" ";
    }


    public static final class AbonentBuilder{
        private String phoneNumber;
        private double balance;
        private String name;

        public AbonentBuilder(){}

        public AbonentBuilder(Abonent copy) {
            this.phoneNumber = copy.phoneNumber;
            this.balance = copy.balance;
            this.name = copy.name;
        }

        public AbonentBuilder setPhoneNumber(String phoneNumber){
            this.phoneNumber=phoneNumber;
            return this;
        }

        public AbonentBuilder setBalance(double balance){
            this.balance=balance;
            return this;
        }

        public AbonentBuilder setName(String name){
            this.name=name;
            return this;
        }

        public Abonent buildAbonent(){
            return new Abonent(this);
        }

    }
}
