import java.util.List;

public class Main {

    public static void main(String[] args) {
        Singleton singleton=Singleton.getInstance();
        File file=singleton.create(args[0]);
        List<Abonent> abonents=file.readAbonent(args[1]+"."+args[0]);
        List<Outgoing> outgoings=file.readOutgoing(args[2]+"."+args[0]);
        Alghoritms alghoritms =new Alghoritms();
        System.out.println("сортировка по имени");
        alghoritms.AbonentSort(abonents);
        System.out.println();
        System.out.println("сортировка по началу звонка");
        alghoritms.CallList(outgoings,abonents);
        System.out.println();
        System.out.println("Вывод баланса с учётом исходящих вызовов");
        alghoritms.Balance(outgoings,abonents);

    }
}
