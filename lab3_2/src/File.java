import java.util.List;

/**
 * Created by Андрей Матвиевич on 08.03.17.
 */
interface File {
    List<Abonent> readAbonent(String file);
    List<Outgoing> readOutgoing(String file);
}
