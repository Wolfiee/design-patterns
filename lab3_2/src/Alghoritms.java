import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Андрей Матвиевич on 09.03.17.
 */

public class Alghoritms {

    public void AbonentSort(List<Abonent> abonents){
        Collections.sort(abonents,new AbonentComparator());
        System.out.println(abonents);
    }

    public void CallList(List<Outgoing> outgoings,List<Abonent> abonentss) {
        Collections.sort(outgoings, new TimeComparator());
        List<Abonent> abonents=new ArrayList<>(abonentss);
        List<Abonent> notEmptyAbonents = new ArrayList<>();
        for (Outgoing o : outgoings) {
            for (Abonent abonent : abonents) {
                if (equal(abonent, o.getAbonent())) {
                    System.out.println(o);
                    if (!notEmptyAbonents.contains(abonent)) notEmptyAbonents.add(abonent);
                }
            }
        }
        notEmptyAbonents.forEach(abonents::remove);
        for (Abonent a : abonents) System.out.println(a + "Нет звонков");
    }

    public void Balance(List<Outgoing> outgoings,List<Abonent> abonents){
        SimpleDateFormat format=new SimpleDateFormat("HH:mm:ss");
        double balance=0;
        List<Abonent> newAbonents=new ArrayList<>();
        for (Abonent abonent : abonents) {
            for (Outgoing o : outgoings) {
                if (equal(abonent, o.getAbonent())) {
                    try {
                        Date begin=format.parse(o.getBegin());
                        Date end=format.parse(o.getEnd());
                        long summary=(end.getTime()-begin.getTime())/ (1000);
                        balance+=summary*o.getPrice()/60;
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
            newAbonents.add(new Abonent.AbonentBuilder(abonent).setBalance(abonent.getBalance()-balance).buildAbonent());
            balance=0;
        }
        System.out.println(newAbonents);

    }

    private boolean equal(Abonent a1,Abonent a2){
        return a1.getName().equals(a2.getName()) && a1.getBalance()==a2.getBalance() && a1.getPhoneNumber().equals(a2.getPhoneNumber());
    }

}
