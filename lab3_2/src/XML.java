import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Андрей Матвиевич on 08.03.17.
 */
@SuppressWarnings("Duplicates")
public class XML implements File {
    @Override
    public List<Abonent> readAbonent(String file) {
        List<Abonent> abonents = new ArrayList<>();
        if(file != null) {
            try {
                InputStream stream = new FileInputStream(file);
                XMLInputFactory factory = XMLInputFactory.newInstance();
                XMLStreamReader reader = factory
                        .createXMLStreamReader(stream);
                Abonent.AbonentBuilder abonent=null;
                while(reader.hasNext()) {
                    int event = reader.next();
                    switch(event) {
                        case XMLStreamReader.START_ELEMENT:
                            switch(reader.getLocalName()) {
                                case "abonent":
                                    abonent = new Abonent.AbonentBuilder();
                                    break;
                                case "phoneNumber":
                                    assert abonent != null;
                                    abonent.setPhoneNumber(reader.getElementText());
                                    break;
                                case "balance":
                                    String balance = reader.getElementText();
                                    double c = Double.parseDouble(balance);
                                    assert abonent != null;
                                    abonent.setBalance(c);
                                    break;
                                case "name":
                                    assert abonent != null;
                                    abonent.setName(reader.getElementText());
                                    break;
                            }
                            break;
                        case XMLStreamReader.END_ELEMENT:

                            if("abonent".equals(reader.getLocalName())) {
                                //  System.out.println(client);
                                abonents.add(abonent.buildAbonent());
                                // System.out.println(abonents);
                            }
                            break;
                    }
                }
                reader.close();
            } catch(FileNotFoundException e) {
                System.err.println("Can't find file \""
                        + file + "\"");
            } catch(XMLStreamException e) {
                System.err.println("Can't read XML-file \""
                        + file + "\"");
            }
        }
        return abonents;
    }

    @Override
    public List<Outgoing> readOutgoing(String file) {
        List<Outgoing> outgoings = new ArrayList<>();
        if(file != null) {
            try {
                InputStream stream = new FileInputStream(file);
                XMLInputFactory factory = XMLInputFactory.newInstance();
                XMLStreamReader reader = factory
                        .createXMLStreamReader(stream);
                Outgoing.OutgoingBuilder outgoingBuilder=null;
                Abonent.AbonentBuilder abonent=null;
                while(reader.hasNext()) {
                    int event = reader.next();
                    switch(event) {
                        case XMLStreamReader.START_ELEMENT:
                            switch(reader.getLocalName()) {
                                case "outgoing":
                                    outgoingBuilder = new Outgoing.OutgoingBuilder();
                                    break;
                                case "abonent":
                                    abonent=new Abonent.AbonentBuilder();
                                    break;
                                case "phoneNumber":
                                    assert abonent != null;
                                    abonent.setPhoneNumber(reader.getElementText());
                                    break;
                                case "balance":
                                    String balance = reader.getElementText();
                                    double c = Double.parseDouble(balance);
                                    assert abonent != null;
                                    abonent.setBalance(c);
                                    break;
                                case "name":
                                    assert abonent != null;
                                    abonent.setName(reader.getElementText());
                                    break;
                                case "date":
                                    assert outgoingBuilder != null;
                                    outgoingBuilder.date(reader.getElementText());
                                    break;
                                case "begin":
                                    assert outgoingBuilder != null;
                                    outgoingBuilder.begin(reader.getElementText());
                                    break;
                                case "end":
                                    assert outgoingBuilder != null;
                                    outgoingBuilder.end(reader.getElementText());
                                    break;
                                case "price":
                                    assert outgoingBuilder != null;
                                    outgoingBuilder.price(Double.parseDouble(reader.getElementText()));
                                    break;
                            }
                            break;
                        case XMLStreamReader.END_ELEMENT:
                            if("abonent".equals(reader.getLocalName())) {
                                assert outgoingBuilder != null;
                                outgoingBuilder.abonent(abonent.buildAbonent());
                                break;
                            }
                            if("outgoing".equals(reader.getLocalName())) {
                                outgoings.add(outgoingBuilder.build());
                            }
                            break;
                    }
                }
                reader.close();
            } catch(FileNotFoundException e) {
                System.err.println("Can't find file \""
                        + file + "\"");
            } catch(XMLStreamException e) {
                System.err.println("Can't read XML-file \""
                        + file + "\"");
            }
        }
        return outgoings;
    }
}
