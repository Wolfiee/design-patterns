import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Андрей Матвиевич on 09.03.17.
 */
public class TimeComparator implements Comparator<Outgoing> {
    @Override
    public int compare(Outgoing o1, Outgoing o2) {
        try {

            Date date1=parce(o1);
            Date date2=parce(o2);
            if(date1.after(date2)) return 1;
            if(date1.before(date2)) return -1;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
    private Date parce(Outgoing o) throws ParseException {
        SimpleDateFormat format=new SimpleDateFormat("dd.MM.YYYY HH:mm:ss");
        return format.parse(o.getDate()+" "+o.getBegin());

    }
}
