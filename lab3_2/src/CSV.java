import com.opencsv.CSVReader;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Андрей Матвиевич on 08.03.17.
 */

public class CSV implements File {
    @Override
    public List<Abonent> readAbonent(String file) {
        List<Abonent> abonents;
        try {
            abonents=new ArrayList<>();
            CSVReader reader=new CSVReader(new FileReader(file),';');
            String [] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                abonents.add(new Abonent.AbonentBuilder()
                        .setName(nextLine[2])
                        .setPhoneNumber(nextLine[0])
                        .setBalance(Double.parseDouble(nextLine[1]))
                        .buildAbonent());
            }
        } catch (Exception e) {
            e.printStackTrace();
            abonents=new ArrayList<>();
        }
        return abonents ;
    }

    @Override
    public List<Outgoing> readOutgoing(String file) {
        List<Outgoing> outgoings;
        try {
            outgoings=new ArrayList<>();
            CSVReader reader=new CSVReader(new FileReader(file),';');
            String [] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                outgoings.add(new Outgoing.OutgoingBuilder().abonent((new Abonent.AbonentBuilder()
                        .setName(nextLine[2])
                        .setPhoneNumber(nextLine[0])
                        .setBalance(Double.parseDouble(nextLine[1]))
                        .buildAbonent())).date(nextLine[3]).begin(nextLine[4]).end(nextLine[5]).price(Double.parseDouble(nextLine[6])).build());
            }
        } catch (Exception e) {
            e.printStackTrace();
            outgoings=new ArrayList<>();
        }
        return outgoings;
    }
}
