import java.util.Comparator;

/**
 * Created by Андрей Матвиевич on 09.03.17.
 */
public class AbonentComparator implements Comparator<Abonent> {

    @Override
    public int compare(Abonent p1, Abonent p2) {
        return p1.getName().compareToIgnoreCase(p2.getName());
    }
}
