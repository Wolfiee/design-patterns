
/**
 * Created by Андрей Матвиевич on 08.03.17.
 */
public class Singleton {
    private static volatile Singleton instance;

    private Singleton(){}

    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance =  new Singleton();
                }
            }
        }
        return instance;
    }
    public File create(String type){
        if(type.equals("xml"))return new XML();
        if(type.equals("csv"))return new CSV();
        return null;
    }
}
