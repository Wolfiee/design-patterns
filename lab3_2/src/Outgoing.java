/**
 * Created by Андрей Матвиевич on 08.03.17.
 */
public class Outgoing {
    private final Abonent abonent;
    private final String date;
    private final String begin;
    private final String end;
    private final double price;

    private Outgoing(OutgoingBuilder builder) {
        abonent = builder.abonent;
        date = builder.date;
        begin = builder.begin;
        end = builder.end;
        price = builder.price;
    }

    public Abonent getAbonent() {
        return abonent;
    }

    public String getDate() {
        return date;
    }

    public String getBegin() {
        return begin;
    }

    public String getEnd() {
        return end;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return  abonent + " " + date + " " + begin + " " + end + " " + price+" ";
    }

    public static final class OutgoingBuilder {
        private Abonent abonent;
        private String date;
        private String begin;
        private String end;
        private double price;

        public OutgoingBuilder() {
        }
        public OutgoingBuilder(Outgoing copy) {
            this.abonent = copy.abonent;
            this.date = copy.date;
            this.begin = copy.begin;
            this.end = copy.end;
            this.price = copy.price;
        }

        public OutgoingBuilder abonent(Abonent abonent) {
            this.abonent = abonent;
            return this;
        }

        public OutgoingBuilder date(String date) {
            this.date = date;
            return this;
        }

        public OutgoingBuilder begin(String begin) {
            this.begin = begin;
            return this;
        }

        public OutgoingBuilder end(String end) {
            this.end = end;
            return this;
        }

        public OutgoingBuilder price(double price) {
            this.price = price;
            return this;
        }

        public Outgoing build() {
            return new Outgoing(this);
        }
    }
}
